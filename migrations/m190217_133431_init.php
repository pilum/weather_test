<?php

use yii\db\Migration;

/**
 * Class m190217_133431_init
 * Создание таблиц расширения и заполнение справочника городов
 */
class m190217_133431_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /**
         * справочник городов
         * outer_id - строка, по которой запрашиваются данные у сервера, предпочтительно использовать числовой ID
         */
        $this->createTable('{{%town}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'outer_id' => $this->string(),
        ]);

        /**
         * таблица с прогнозами
         */
        $this->createTable('{{%forecast}}', [
            'town_id' => $this->integer()->notNull(),
            'forecast_date' => $this->date()->notNull(),
            'temperature' => $this->decimal(4, 2)->notNull(),
        ]);
        $this->addPrimaryKey('{{%forecast_PK}}', '{{%forecast}}', [
            'town_id',
            'forecast_date'
        ]);
        $this->addForeignKey('{{%forecast_town_FK}}', '{{%forecast}}', ['town_id'],
            '{{%town}}', 'id');

        // заполнение начального списка городов c идентификаторами для конкретного API
        if (!\Yii::$app->weatherForecastService) {
            echo 'Компонент weatherForecastService не настроен, начальный список городов не был заполнен';
        } else {
            $this->batchInsert('{{%town}}', ['id', 'name', 'outer_id'],
                \Yii::$app->weatherForecastService::getInitTowns()
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%forecast}}');
        $this->dropTable('{{%town}}');
    }
}
