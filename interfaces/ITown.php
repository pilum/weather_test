<?php

namespace app\interfaces;

/**
 * интерфейс для уменьшения связности между компонентом работы с прогнозами и моделью города
 * Interface ITown
 * @package app\interfaces
 */
interface ITown
{
    /**
     * идентификатор города во внутренней БД
     * @return int
     */
    public function getId(): int;

    /**
     * идентификатор города в терминах API сервиса
     * @return string
     */
    public function getOuterId(): string;
}