<?php

namespace app\models;

use app\interfaces\ITown;

/**
 * This is the model class for table "town".
 * справочник городов
 *
 * @property int $id
 * @property string $name
 * @property string $outer_id
 */
class Town extends \yii\db\ActiveRecord implements ITown
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'town';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'outer_id'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'outer_id' => 'Идентификатор в API',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getOuterId(): string
    {
        return $this->outer_id;
    }
}
