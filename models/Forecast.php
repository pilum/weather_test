<?php

namespace app\models;

/**
 * This is the model class for table "forecast".
 * история прогнозов погоды
 *
 * @property int $town_id
 * @property string $forecast_date
 * @property string $temperature
 */
class Forecast extends \yii\db\ActiveRecord
{
    public $townName;
    public $delta;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forecast';
    }

    /**
     * сохранение новых данных прогноза
     * @param array $newData
     * @throws \yii\db\Exception
     */
    public static function addNewData(array $newData)
    {
        $sql =
            'INSERT INTO forecast(town_id, forecast_date, temperature)
              VALUES (:town_id, :forecast_date, :temperature)
              ON CONFLICT (town_id, forecast_date) DO
              UPDATE SET temperature = EXCLUDED.temperature';

        $command = \Yii::$app->db->createCommand($sql);
        $command->prepare();
        foreach ($newData as $forecast) {
            $command->bindValues([
                ':town_id' => $forecast['town_id'],
                ':forecast_date' => $forecast['forecast_date'],
                'temperature' => $forecast['temperature']
            ]);
            $command->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['town_id', 'forecast_date', 'temperature'], 'required'],
            [['town_id'], 'default', 'value' => null],
            [['town_id'], 'integer'],
            [['forecast_date'], 'safe'],
            [['temperature'], 'number'],
            [['town_id', 'forecast_date'], 'unique', 'targetAttribute' => ['town_id', 'forecast_date']],
            [
                ['town_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Town::className(),
                'targetAttribute' => ['town_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'town_id' => 'ID города',
            'forecast_date' => 'Дата',
            'temperature' => 'Температура',
            'delta' => 'Разница'

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTown()
    {
        return $this->hasOne(Town::className(), ['id' => 'town_id']);
    }

    /**
     * @return string
     */
    public function getTownName()
    {
        return $this->town->name;
    }
}