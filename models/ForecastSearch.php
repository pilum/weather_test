<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * ForecastSearch represents the model behind the search form of `app\models\Forecast`.
 */
class ForecastSearch extends Forecast
{
    /**
     * @var string дата начала периода
     */
    public $dateFrom;
    /**
     * @var string дата конца периода
     */
    public $dateTo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['town_id'], 'integer'],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:d.m.Y'],
            [['temperature'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'town_id' => 'ID города',
            'forecast_date' => 'Дата',
            'temperature' => 'Температура',
            'dateFrom' => 'С',
            'dateTo' => 'По',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Forecast::find()->select([
            'townName' => 'town.name',
            'forecast_date' => 'forecast.forecast_date',
            'temperature' => 'forecast.temperature',
            'delta' => new Expression(
                'forecast.temperature - 
                lag(forecast.temperature, 1, forecast.temperature) 
                over(partition by forecast.town_id order by forecast.forecast_date)'),
        ])
            ->joinWith(['town'], true, 'INNER JOIN');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['townName'] = [
            'asc' => [Town::tableName() . '.name' => SORT_ASC],
            'desc' => [Town::tableName() . '.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['delta'] = [
            'asc' => ['delta' => SORT_ASC],
            'desc' => ['delta' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->dateFrom) {
            $query->andWhere([
                '>=',
                'forecast_date',
                date_create_from_format('d.m.Y', $this->dateFrom)->format('Y-m-d')
            ]);
        }
        if ($this->dateTo) {
            $query->andWhere([
                '<=',
                'forecast_date',
                date_create_from_format('d.m.Y', $this->dateTo)->format('Y-m-d')
            ]);
        }
        return $dataProvider;
    }
}
