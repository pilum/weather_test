<?php

namespace app\commands;

use app\models\Forecast;
use yii\console\Controller;

/**
 * Обновления прогноза с получением данных через API метео-сервиса
 */
class GetForecastController extends Controller
{
    /**
     * Запрос обновления прогноза
     *
     */
    public function actionIndex()
    {
        echo date('Y-m-d H:i:s') . ' Старт получения данных' . PHP_EOL;
        \Yii::info('Старт получения данных');
        try {
            if ($forecast = \Yii::$app->weatherForecastService->getForecast()) {
                Forecast::addNewData($forecast);
            }
        } catch (\Exception|\Error $e) {
            echo date('Y-m-d H:i:s') . ' Ошибка при получении прогноза: ' . $e->getMessage() . PHP_EOL;
            \Yii::error('Ошибка при получении прогноза: ' . $e->getMessage());                        
        }

        echo date('Y-m-d H:i:s') . ' Получение данных завершено' . PHP_EOL;
        \Yii::info('Получение данных завершено');
    }
}