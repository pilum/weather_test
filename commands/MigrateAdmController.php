<?php

namespace app\commands;

use Yii;
use yii\console\controllers\MigrateController;
use yii\db\Exception;
use yii\helpers\Console;

/**
 * Замена контроллера миграции для повышения безопасности, подразумевается, что пользователь, от имени
 * которого запускается обработка запросов, не имеет прав на DDL, и нам надо подключиться под админом
 * @package app\commands
 */
class MigrateAdmController extends MigrateController
{
    /**
     * @inheritdoc
     * @throws \yii\base\ExitException
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        // обрабатываем только изменения БД
        if (!in_array($action->id, ['up', 'down', 'redo', 'to', 'fresh'])) {
            return true;
        }
        // Для работы с БД запрашиваем данные администратора
        $admin = Console::input('Введите логин администратора БД: ');
        Console::stdout('Введите пароль: ');
        // тут есть недостаток: при нажатии Ctrl+C в момент ввода пароля, цвет шрифта не возвращается,
        // у пользователя возникает подозрение о системном глюке
        Console::beginAnsiFormat([
            Console::BG_GREY,
            Console::FG_GREY
        ]);
        $pass = Console::input();
        Console::endAnsiFormat();
        Console::moveCursorUp(2);
        Console::clearScreenAfterCursor();


        Yii::$app->db->close();
        Yii::$app->db->username = $admin;
        Yii::$app->db->password = $pass;
        try {
            Yii::$app->db->open();
        } catch (Exception $e) {
            Console::output('Ошибка подключения к БД: ' . $e->getMessage());
            Yii::$app->end();
        }
        return true;
    }
}