<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\Pjax;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $sedarchModel app\models\ForecastSearch */
/* @var $this yii\web\View */

$this->title = 'Изменения в температурах';

\Yii::$app->formatter->dateFormat = 'php:d.m.Y';
?>
<div class="forecast-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <div class="forecast-form">

        <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'GET']); ?>

        <?= $form->field($searchModel, 'dateFrom')->widget(DatePicker::className()); ?>

        <?= $form->field($searchModel, 'dateTo')->widget(DatePicker::className()); ?>


        <div class="form-group">
            <?= Html::submitButton('Применить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php \yii\widgets\ActiveForm::end(); ?>

    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'townName',
                'label' => 'Город',
            ],
            [
                'attribute' => 'forecast_date',
                'format' => ['date', 'php:d.m.Y']
            ],
            'temperature',
            'delta'
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>