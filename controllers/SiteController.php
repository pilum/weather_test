<?php

namespace app\controllers;

use app\models\ForecastSearch;
use yii\web\Controller;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Lists all Forecast models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ForecastSearch();
        // print_r(\Yii::$app->request);
        $dataProvider = $searchModel->search($_REQUEST);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
