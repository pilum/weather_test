<?php
/**
 * компонент для работы с сервисом OpenWeatherMap.org
 */

namespace app\components;


use yii\httpclient\Response;

class OpenWeather extends WeatherForecastService
{
    /**
     * @inheritdoc
     */
    public static function getInitTowns(): array
    {
        return [
            [1, 'Москва', '524901'],
            [2, 'Санкт-Петербург', '536203'],
            [3, 'Нижний Новгород', '524901'],
        ];
    }

    /**
     * генерация строки обращения к конкретному API
     * @param string $outerId
     * @return string
     */
    protected function createUrl(string $outerId): string
    {
        return "http://api.openweathermap.org/data/2.5/forecast?units=metric&appid={$this->apiKey}&id={$outerId}";
    }

    /**
     * Фильтрация и конвертация данных полученных от API во внутренний формат
     * @param Response $response объект ответа от сервиса
     * @param int $townInnerId идентификатор города в БД
     * @return array
     * @throws \Exception
     */
    protected function decode(Response $response, int $townInnerId): array
    {
        if (!$response->isOk) {
            throw new \Exception("Ошибка при обращении к API: {$response->statusCode} - {$response->getData()['message']}");
        }

        if (!isset($response->data['list'])) {
            throw new \Exception('Неверный формат данных!');
        }

        $data = [];
        foreach ($response->data['list'] as $row) {
            /* сервис бесплатно отдает данные только c разницей в 3 часа за ближайшие 3 суток,
            я решил считать температурой дня ту, которая берется в полдень */
            if ((date('H:i:s', (int)$row['dt']) !== '12:00:00')) {
                continue;
            }
            $data[] = [
                'town_id' => $townInnerId,
                'forecast_date' => date('Y-m-d', (int)$row['dt']),
                'temperature' => $row['main']['temp'],
            ];
        }
        return $data;
    }
}