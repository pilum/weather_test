<?php

namespace app\components;

use yii\base\Component;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Базовый класс для доступа к API прогнозного сервиса
 * Class WeatherForecastService
 * @package app\components
 */
abstract class WeatherForecastService extends Component
{
    /**
     * ключ/токен для доступа к API
     * @var string
     */
    protected $apiKey = '';

    /**
     * массив городов, для которых получаем прогноз
     * @var array of ITown
     */
    protected $activeTowns = [];

    /**
     * WeatherForecastService constructor.
     * @param string $key ключ/токен для доступа к API
     * @param array of ITown $activeTowns массив городов для отслеживания погоды
     */
    public function __construct(string $key, array $activeTowns)
    {
        parent::__construct([]);
        $this->apiKey = $key;
        $this->activeTowns = $activeTowns;
    }

    /**
     * Начальный набор городов с идентификаторами характерными для конкрнетного прогнозного сервиса
     * @return array
     */
    public abstract static function getInitTowns(): array;

    /**
     * Получение свежего прогноза
     * @throws \Exception
     */
    public function getForecast()
    {
        $result = [];
        if (is_array($this->activeTowns) && !empty($this->activeTowns)) {
            foreach ($this->activeTowns as $town) {
                foreach ($this->decode($this->getForecastForTown($town->getOuterId()), $town->getId()) as $row) {
                    $result[] = $row;
                }
            }
            return $result;
        } else {
            throw new \Exception('Список городов не заполнен!');
        }
    }

    /**
     * Фильтрация и конвертация данных полученных от API во внутренний формат
     * @param Response $response объект ответа от сервиса
     * @param int $townInnerId идентификатор города в БД
     * @return array
     */
    protected abstract function decode(Response $response, int $townInnerId): array;

    /**
     * Получение прогноза для одного города
     * @param string $outerId
     * @return Response
     * @throws \Exception
     */
    protected function getForecastForTown(string $outerId): Response
    {
        $client = new Client();
        return $client->get($this->createUrl($outerId), null, $this->createHeaders($outerId))->send();
    }

    /**
     * генерация строки обращения к конкретному API
     * @param string $outerId
     * @return string
     */
    protected abstract function createUrl(string $outerId): string;

    /**
     * заголовки для обращения к API (например, для gismeteo так передается токен)
     * @param $outerId string
     * @return array
     */
    protected function createHeaders(string $outerId): array
    {
        return [];
    }
}